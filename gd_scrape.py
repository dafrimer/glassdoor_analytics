from lxml import etree
from selenium import webdriver
import time
import pandas as pd
ZZZ = 50
#check for signed in
SIGNEDIN = False
import datetime

START_PAGE = 'https://www.glassdoor.com/Reviews/All-Star-Directories-Inc-Reviews-E234717.htm?sort.sortType=RD&sort.ascending=false&filter.employmentStatus=REGULAR&filter.employmentStatus=PART_TIME&filter.employmentStatus=UNKNOWN'
d = webdriver.Chrome('../chromedriver.exe')

d.get(START_PAGE)

try:
    d.find_element_by_xpath('//div[@id="LoginModule-GPlusBtnSignUp"]').click()
    time.sleep(ZZZ)
    d.find_element_by_xpath('//input[@type="email"]').send_keys(UNAME)

except:
    pass

alldata = []
nextPage = START_PAGE
while True:
    d.get(nextPage)
    # Click the "Show More"'s
    for i in d.find_elements_by_class_name('moreEllipses'):
        try:
            i.click()
        except:
            pass
            #probably not visible

    root = etree.fromstring(d.page_source, etree.HTMLParser())

    for single in root.xpath('//div[@class="hreview"]'):
        sj = {}
        try:
            sj['Date'] = single.xpath('.//time')[0].attrib['datetime']
        except:
            continue
        sj['Title'] = single.xpath('.//a[@class="reviewLink"]/span')[0].text
        sj['Rating'] = single.xpath('.//span[@class="value-title"]/@title')[0]
        sj['Status'] = single.xpath('.//span[@class="authorJobTitle middle reviewer"]/text()')[0]

        try:
            sj['1 Rec'] = "".join(
                single.xpath('.//div[@class="flex-grid recommends"]//span[@class="middle"]')[0].itertext())
        except:
            pass
        try:
            sj['2 Rec'] = "".join(
                single.xpath('.//div[@class="flex-grid recommends"]//span[@class="middle"]')[1].itertext())
        except:
            pass
        try:
            sj['3 Rec'] = "".join(
                single.xpath('.//div[@class="flex-grid recommends"]//span[@class="middle"]')[2].itertext())
        except:
            pass


        try:
            sj['1 Comment'] = " ".join(single.xpath('.//div[./p/@class = "strong tightVert"]')[0].itertext()).strip()
        except:
            pass
        try:
            sj['2 Comment'] = " ".join(single.xpath('.//div[./p/@class = "strong tightVert"]')[1].itertext()).strip()
        except:
            pass
        try:
            sj['3 Comment'] = " ".join(single.xpath('.//div[./p/@class = "strong tightVert"]')[2].itertext()).strip()
        except:
            pass

        alldata.append(sj)
    try:
        nextPage = 'https://www.glassdoor.com' + root.xpath('//a[i/span/text() = "Next"]')[0].attrib['href']
    except:
        break

df = pd.DataFrame(alldata)
####### Clean Table
def pros(cols):
    for c in cols:
        if pd.isnull(c):
            continue
        if re.search('^Pros.*', c):
            return c
    return pd.np.nan


def cons(cols):
    for c in cols:
        if pd.isnull(c):
            continue
        if re.search('^Cons.*', c):
            return c
    return pd.np.nan


def mgmt(cols):

    for c in cols:
        if pd.isnull(c):
            continue
        if re.search('^Advice to Management.*', c):
            return c
    return pd.np.nan
df['Pros'] = df[['1 Comment','2 Comment','3 Comment']].apply(lambda x : pros(x) ,axis=1)
df['Cons'] = df[['1 Comment','2 Comment','3 Comment']].apply(lambda x : cons(x) ,axis=1)
df['Mgmt'] = df[['1 Comment','2 Comment','3 Comment']].apply(lambda x : mgmt(x) ,axis=1)

df.Mgmt = df.Mgmt.apply(lambda x : x[len('Advice To Management '):].strip() if pd.notnull(x) else pd.np.nan)
df.Pros = df.Mgmt.apply(lambda x : x[len('Pros '):].strip() if pd.notnull(x) else pd.np.nan)
df.Cons = df.Mgmt.apply(lambda x : x[len('Cons '):].strip() if pd.notnull(x) else pd.np.nan)



def recs(cols):
    for c in cols:
        if pd.isnull(c):
            continue
        if 'Recommend' in c:
            return c
    return pd.np.nan


def outlook(cols):
    for c in cols:
        if pd.isnull(c):
            continue
        if 'Outlook' in c:
            return c
    return pd.np.nan


def ceo(cols):
    for c in cols:
        if pd.isnull(c):
            continue
        if 'CEO' in c:
            return c
    return pd.np.nan


df['CEO'] = df[['1 Rec','2 Rec','3 Rec']].apply(lambda x : ceo(x),axis=1)
df['Recommends'] = df[['1 Rec','2 Rec','3 Rec']].apply(lambda x : recs(x),axis=1)
df['Outlook'] = df[['1 Rec','2 Rec','3 Rec']].apply(lambda x : outlook(x),axis=1)


df.Date = df.Date.astype(datetime.datetime)
df.Rating = df.Rating.astype(float)


df.to_csv('./glassdoorallstar.csv', sep=',', index=False, quoting=1)


